#-*-coding:utf-8-*-
'''
Файл с переводом
'''
#
#from locale import getdefaultlocale
#LANGUAGE = getdefaultlocale()[0]
#print LANGUAGE
#Russian
ERROR_LOADING_MODULES="Не могу загрузить модуль"
ERROR_LOADING_IMAGE="Не могу загрузить изображение"
ERROR_LOADING_SOUND="Не могу загрузить звуковой файл"
CREATE_GAME_CLASS="Создаем класс игры"
FULLSCREEN_ENABLE="Полноэкранный режим"
FULLSCREEN_DISABLE="Не полный экран"
GOOD_BY="Пока"
PAUSE_ENABLE="Игра на паузе"
PAUSE_DISABLE="Игра на паузе"

GAME_CAPTION="Balloons"
TXT_SCORE="Очки"
TXT_LEVEL="Уровень"
TXT_LIFE="Жизнь"
